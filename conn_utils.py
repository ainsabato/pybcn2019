"""
Copyright 2018 Andrea Insabato, Matthieu Gilson
This code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Utility functions and classes to deal with network construction and estimation.
This file is meant to be used with the notebook pyBCN_talk.ipynb.
"""

import numpy as np
import networkx as nx
import scipy.linalg as spl
import scipy.stats as stt
import matplotlib.pyplot as plt
from sklearn.base import BaseEstimator
from sklearn.covariance import GraphicalLasso
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as pp
from matplotlib.gridspec import GridSpec
import seaborn as sns


def make_chain():
    chain = nx.DiGraph()
    chain.add_nodes_from(['X', 'Y', 'Z'])
    chain.add_edges_from([('Y', 'Z'), ('Z', 'X')])
    return chain

def make_common_input():
    common_drive = nx.DiGraph()
    common_drive.add_nodes_from(['X', 'Y', 'Z'])
    common_drive.add_edges_from([('Z', 'X'), ('Z', 'Y')])
    return common_drive

def make_full():
    full = nx.DiGraph()
    full.add_nodes_from(['X', 'Y', 'Z'])
    full.add_edges_from([('X', 'Z'), ('Y', 'Z'), ('X', 'Y'), ('Z', 'Y'), ('Y', 'X'), ('Z', 'X')])
    return full

def make_rnd_connectivity(n, density=0.2, connectivity_strength=0.5):
    """
    Creates a random connnectivity matrix as the element-wise product $ C' = A \otimes W$,
    where A is a binary adjacency matrix samples from Bern(density) and W is sampled from log-normal
    $log(W) \sim \mathcal{N}(0,1)$.
    The matrix gets normalized in order to avoid explosion of activity when varying the number of nodes n.
    $ C = \frac{C' n}{\sum_{i,j} C'} $
    """
    C = np.exp(np.random.randn(n, n))  # log normal
    C[np.random.rand(n, n) > density] = 0
    C[np.eye(n, dtype=bool)] = 0
    C *= connectivity_strength * n / C.sum()
    return C

def plot_CVfold(k, X, train, test, i):
    N = len(X)
    plt.subplot(k, 1, i+1)
    plt.scatter(X[train], np.repeat(1, len(X[train])), marker='s', s=20000/N**2, c='pink', label='train')
    plt.scatter(X[test], np.repeat(1, len(X[test])), marker='s', s=20000/N**2, c='green', label='test')
    plt.yticks([])
    plt.xticks([])
    sns.despine(left=True)
    plt.ylabel('fold %i' %(i+1), rotation=0, ha='right')


class MOU(BaseEstimator):
    
    def __init__(self, n_nodes=10, C=None, Sigma=None, tau_x=1.0, mu=0.0, random_state=None):
        if random_state is not None:  # set seed for reproducibility
            np.random.seed(random_state)
        if C is None:
            self.C = np.zeros([n_nodes, n_nodes])
        else:
            self.C = C
        if Sigma is None:
            self.Sigma = np.eye(n_nodes) * 0.5 + np.eye(n_nodes) * 0.5 * np.random.rand(n_nodes)
        elif np.isscalar(Sigma):
            self.Sigma = np.eye(n_nodes) * np.sqrt(Sigma)
        else:
            raise("Only scalar values accepted corresponding to diagonal noise covariance matrix.")
        self.tau_x = tau_x
        self.n_nodes = n_nodes
        self.mu = mu

    def fit(self, X, y=None, SC_mask=None, method='lyapunov', norm_fc=None, 
            epsilon_EC=0.0005, epsilon_Sigma=0.05, min_val_EC=0.0, max_val_EC=0.4,
            n_opt=10000, regul_EC=0, regul_Sigma=0):
        """
        Estimation of MOU parameters (connectivity C, noise covariance Sigma,
        and time constant tau_x) with Lyapunov optimization as in: Gilson et al.
        Plos Computational Biology (2016).
        PARAMETERS:
            X: the timeseries data of the system to estimate, shape: T time points x P variables.
            y: needed to comply with BaseEstimator (not used here)
            SC_mask: mask of known non-zero values for connectivity matrix, for example 
            estimated by DTI
            method: 'lyapunov' or 'moments'
            norm_fc: normalization factor for FC. Normalization is needed to avoid high connectivity value that make
            the network activity explode. FC is normalized as FC *= 0.5/norm_fc. norm_fc can be specified to be for example
            the average over all entries of FC for all subjects or sessions in a given group. If not specified the normalization factor is
            the mean of 0-lag covariance matrix of the provided time series ts_emp. 
            epsilon_EC : learning rate for connectivity (this should be about n_nodes times smaller than epsilon_Sigma).
            epsilon_Sigma : learning rate for Sigma (this should be about n_nodes times larger than epsilon_EC).
            min_val_EC : minimum value to bound connectivity estimate. This should be zero or slightly negative (too negative limit can bring to an
            inhibition dominated system). If the empirical covariance has many negative entries then a slightly negative limit can improve the estimation
            accuracy.
            max_val_EC : maximum value to bound connectivity estimate. This is useful to avoid large weight that make the system unstable.
            If the estimated connectivity saturates toward this value (it usually doesn't happen) it can be increased.
            n_opt : number of maximum optimization steps. If final number of iterations reaches this maximum it means the algorithm has not converged.
            regul_EC : regularization parameter for connectivity (try a value of 0.5)
            regul_Sigma : regularization parameter for Sigma (try a value of 0.001)
        RETURN:
            C: estimated connectivity matrix, shape [P, P] with null-diagonal
            Sigma: estimated noise covariance, shape [P, P]
            tau_x: estimated time constant (scalar)
            d_fit: a dictionary with diagnostics of the fit; keys are: iterations, distance and correlation
        """
        # TODO: raise a warning if the algorithm does not converge
        # TODO: look into regularization
        # TODO: move SC_make to __init__()
        # TODO: make better graphics (deal with axes separation, etc.)
        # TODO: check consistent N between object init and time series X passed to fit()

        n_T = X.shape[0]  # number of time samples
        N = X.shape[1] # number of ROIs
        d_fit = dict()  # a dictionary to store the diagnostics of fit
        
        # mask for existing connections for EC and Sigma
        mask_diag = np.eye(N,dtype=bool)
        if SC_mask is None:
            mask_EC = np.logical_not(mask_diag) # all possible connections except self
        else:
            mask_EC = SC_mask
        mask_Sigma = np.eye(N,dtype=bool) # independent noise
        #mask_Sigma = np.ones([N,N],dtype=bool) # coloured noise
            
        if method=='lyapunov':
            
            n_tau = 3 # number of time shifts for FC_emp
            v_tau = np.arange(n_tau)
            i_tau_opt = 1 # time shift for optimization
            
            min_val_Sigma_diag = 0. # minimal value for Sigma
          
            # FC matrix 
            ts_emp = X - np.outer(np.ones(n_T), X.mean(0))
            FC_emp = np.zeros([n_tau,N,N])
            for i_tau in range(n_tau):
                FC_emp[i_tau,:,:] = np.tensordot(ts_emp[0:n_T-n_tau,:],ts_emp[i_tau:n_T-n_tau+i_tau,:],axes=(0,0)) / float(n_T-n_tau-1)
        
            # normalize covariances (to ensure the system does not explode)
            if norm_fc is None:
                norm_fc = FC_emp[0,:,:].mean()
            FC_emp *= 0.5/norm_fc
        
            # autocovariance time constant
            log_ac = np.log(np.maximum(FC_emp.diagonal(axis1=1,axis2=2),1e-10))
            lin_reg = np.polyfit(np.repeat(v_tau,N),log_ac.reshape(-1),1)
            tau_x = -1./lin_reg[0]
            
            # optimization
            tau = v_tau[i_tau_opt]
            
            # objective FC matrices (empirical)
            FC0_obj = FC_emp[0,:,:]
            FCtau_obj = FC_emp[i_tau_opt,:,:]
            
            coef_0 = np.sqrt(np.sum(FCtau_obj**2)) / (np.sqrt(np.sum(FC0_obj**2))+np.sqrt(np.sum(FCtau_obj**2)))
            coef_tau = 1. - coef_0
            
            # initial network parameters
            EC = np.zeros([N,N])
            Sigma = np.eye(N)  # initial noise
            
            # best distance between model and empirical data
            best_dist = 1e10
            best_Pearson = 0.
            
            # record model parameters and outputs
            dist_FC_hist = np.zeros([n_opt])*np.nan # FC error = matrix distance
            Pearson_FC_hist = np.zeros([n_opt])*np.nan # Pearson corr model/objective
            
            stop_opt = False
            i_opt = 0
            while not stop_opt:
                # calculate Jacobian of dynamical system
                J = -np.eye(N)/tau_x + EC
                		
                # calculate FC0 and FCtau for model
                FC0 = spl.solve_lyapunov(J,-Sigma)
                FCtau = np.dot(FC0,spl.expm(J.T*tau))
                
                # calculate error between model and empirical data for FC0 and FC_tau (matrix distance)
                err_FC0 = np.sqrt(np.sum((FC0-FC0_obj)**2))/np.sqrt(np.sum(FC0_obj**2))
                err_FCtau = np.sqrt(np.sum((FCtau-FCtau_obj)**2))/np.sqrt(np.sum(FCtau_obj**2))
                dist_FC_hist[i_opt] = 0.5*(err_FC0+err_FCtau)
                	
                # calculate Pearson corr between model and empirical data for FC0 and FC_tau
                Pearson_FC_hist[i_opt] = 0.5*(stt.pearsonr(FC0.reshape(-1),FC0_obj.reshape(-1))[0]+stt.pearsonr(FCtau.reshape(-1),FCtau_obj.reshape(-1))[0])
                
                # best fit given by best Pearson correlation coefficient for both FC0 and FCtau (better than matrix distance)
                if dist_FC_hist[i_opt]<best_dist:
                    	best_dist = dist_FC_hist[i_opt]
                    	best_Pearson = Pearson_FC_hist[i_opt]
                    	i_best = i_opt
                    	EC_best = np.array(EC)
                    	Sigma_best = np.array(Sigma)
                    	FC0_best = np.array(FC0)
                    	FCtau_best = np.array(FCtau)
                else:
                    stop_opt = i_opt>100
                
                # Jacobian update with weighted FC updates depending on respective error
                Delta_FC0 = (FC0_obj-FC0)*coef_0
                Delta_FCtau = (FCtau_obj-FCtau)*coef_tau
                Delta_J = np.dot(np.linalg.pinv(FC0),Delta_FC0+np.dot(Delta_FCtau,spl.expm(-J.T*tau))).T/tau
                # update conectivity and noise
                EC[mask_EC] += epsilon_EC * (Delta_J - regul_EC*EC)[mask_EC]
                EC[mask_EC] = np.clip(EC[mask_EC],min_val_EC,max_val_EC)
                
                Sigma[mask_Sigma] += epsilon_Sigma * (-np.dot(J,Delta_FC0)-np.dot(Delta_FC0,J.T) - regul_Sigma)[mask_Sigma]
                Sigma[mask_diag] = np.maximum(Sigma[mask_diag],min_val_Sigma_diag)
                
                # check if end optimization: if FC error becomes too large
                if stop_opt or i_opt==n_opt-1:
                    stop_opt = True
                    d_fit['iterations'] = i_opt
                    d_fit['distance'] = best_dist
                    d_fit['correlation'] = best_Pearson
                else:
                    i_opt += 1

        elif method=='moments':
            n_tau = 2
            ts_emp = X - np.outer(np.ones(n_T), X.mean(0))  # subtract mean
            # empirical covariance (0 and 1 lagged)
            Q_emp = np.zeros([n_tau, self.n_nodes, self.n_nodes])
            for i_tau in range(n_tau):
                Q_emp[i_tau, :, :] = np.tensordot(ts_emp[0:n_T-n_tau,:],ts_emp[i_tau:n_T-n_tau+i_tau,:],axes=(0,0)) / float(n_T-n_tau-1)
            # Jacobian estimate
            J = spl.logm(np.dot(np.linalg.inv(Q_emp[0, :, :]), Q_emp[1, :, :])).T  # WARNING: tau is 1 here (if a different one is used C gets divided by tau)
            if np.any(np.iscomplex(J)):
                J = np.real(J)
                print("Warning: complex values in J; casting to real!")
            # Sigma estimate
            Sigma_best = -np.dot(J, Q_emp[0, :, :])-np.dot(Q_emp[0, :, :], J.T)
            # theoretical covariance
            Q0 = spl.solve_lyapunov(J, -Sigma_best)
            # theoretical 1-lagged covariance
            Qtau = np.dot(Q0, spl.expm(J.T))  # WARNING: tau is 1 here (if a different one is used J.T gets multiplied by tau)
            # average correlation between empirical and theoretical
            d_fit['correlation'] = 0.5 * (stt.pearsonr(Q0.flatten(), Q_emp[0, :, :].flatten())[0] +
                                         stt.pearsonr(Qtau.flatten(), Q_emp[1, :, :].flatten())[0])
            tau_x = -J.diagonal().copy()
            EC_best = np.zeros([N, N])
            EC_best[mask_EC] = J[mask_EC]

        else:
            raise('method should be either \'lyapunov\' or \'moments\'')

        self.C = EC_best
        self.Sigma = Sigma_best
        self.tau_x = tau_x
        self.d_fit = d_fit

        return self

    def score(self):
        try:
            return self.d_fit['correlation']
        except:
            print('the model has not been fit yet. Call the fit method first.')
            
    def model_covariance(self, tau=0):
        """
        Calculates theoretical (lagged) covariances of the model given the parameters (forward step).
        Notice that this is not the empirical covariance matrix as estimated from simulated time series.
        PARAMETERS:
            tau : the lag to calculate the covariance
        RETURNS:
            FC : the (lagged) covariance matrix.
        """
        J = -np.eye(self.n_nodes)/self.tau_x + self.C
        # calculate FC0 and FCtau for model
        FC0 = spl.solve_lyapunov(J, -self.Sigma)
        if tau==0:
            return FC0
        else:
            FCtau = np.dot(FC0, spl.expm(J.T * tau))
            return FCtau

    def simulate(self, T=9000, dt=0.05, random_state=None):
        """
        Simulate the model with simple Euler integration.
        -----------
        PARAMETERS:
        T : duration of simulation
        dt : integration time step
        -----------
        RETURNS:
        ts : time series of simulated network activity of shape [T, n_nodes]
        -----------
        NOTES:
        it is possible to include an acitvation function to
        give non linear effect of network input; here assumed to be identity

        """
        if random_state is not None:  # set seed for reproducibility
            np.random.seed(random_state)
        T0 = 100.  # initialization time for network dynamics
        n_sampl = int(1./dt)  # sampling to get 1 point every second
        n_T = int(np.ceil(T/dt))
        n_T0 = int(T0/dt)
        ts = np.zeros([n_T, self.n_nodes])  # to save results
        # initialization
        t_span = np.arange(n_T0 + n_T, dtype=int)
        x_tmp = np.random.rand(self.n_nodes)  # initial activity of nodes
        u_tmp = np.zeros([self.n_nodes])
        # sample all noise terms at ones
        noise = np.random.normal(size=[n_T0 + n_T, self.n_nodes], scale=(dt**0.5))
        # numerical simulations
        for t in t_span:
            u_tmp = np.dot(self.C, x_tmp) + self.mu
            x_tmp += dt * (-x_tmp / self.tau_x + u_tmp) + np.dot(self.Sigma, noise[t, :])
            if t > (n_T0-1):  # discard first n_T0 timepoints
                ts[t-n_T0, :] = x_tmp

        # subsample timeseries to approx match fMRI time resolution
        return ts[::n_sampl, :]

